const express = require('express');
const helmet = require('helmet'); // security
const morgan = require('morgan');
const path = require('path');
const handlebars = require('express-handlebars');
const methodOverride = require('method-override');
const dotenv = require('dotenv');
dotenv.config();
const { connectDatabase } = require("../src/configs/db/db.config");

const { sortMidleware } = require('../src/app/middlewares/sortMidleware');

const app = express();

const routes = require('./routes');
connectDatabase();

app.use(express.static(path.join(__dirname, 'public')));

app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(methodOverride('_method'));

// Custom middlewares
app.use(sortMidleware);

// HTTP logger
app.use(morgan('combined'));

//template engine
app.engine(
    'hbs',
    handlebars({
        extname: '.hbs',
        helpers: require('./helpers/handlebars')
    }),
);
app.set('view engine', 'hbs');
app.set('views', path.join(__dirname, 'resources/views'));
//

//route
routes(app);

const port = process.env.PORT || 2001;

app.listen(port, () => {
    console.log('Successfully with port: ' + port);
});
