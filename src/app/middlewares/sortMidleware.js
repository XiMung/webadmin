
// module.exports = function SortMidleware(req, res, next) {
//     res.local._sort = {
//         enabled: false,
//         type: 'default'
//     }

//     if (req.query.hasOwnProperty('_sort')) {
//         res.local._sort.enabled = true;
//         res.local._sort.type = req.query.type;
//         res.local._sort.column = req.query.column;
//     }

//     next();
// }

const sortMidleware = (req, res, next) => {
    res.locals._sort = {
        enabled: false,
        type: 'default'
    }

    if (req.query.hasOwnProperty('_sort')) {
        res.locals._sort.enabled = true;
        res.locals._sort.type = req.query.type;
        res.locals._sort.column = req.query.column;
    }

    next();
}

module.exports = {sortMidleware: sortMidleware}