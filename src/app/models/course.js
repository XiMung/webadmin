const mongoose = require("mongoose");
const AutoIncrement = require('mongoose-sequence')(mongoose);
const slug = require('mongoose-slug-generator');
const mongooseDelete = require('mongoose-delete');
const Schema = mongoose.Schema;

const Course = Schema({
    _id: { type: Number},
    name: { type: String, required: true },
    description: { type: String },
    image: { type: String },
    video: { type: String },
    level: { type: String },
    slug: { type: String, slug: "name", unique: true },
}, {
    _id: false,
    timestamps: true
});

// custom query helpers
Course.query.sortable = function (req) {
    if (req.query.hasOwnProperty('_sort')) {
        const isValueType = ['asc', 'desc'].includes(req.query.type);
        return this.sort({
            [req.query.column]: isValueType ? req.query.type : 'desc'
        })
    }
    return this;
}

//add plugin
mongoose.plugin(slug);

// tự tăng field _id
Course.plugin(AutoIncrement)

Course.plugin(mongooseDelete, {
    deletedAt: true,
    overrideMethods: 'all'
});

module.exports = mongoose.model("course", Course);
