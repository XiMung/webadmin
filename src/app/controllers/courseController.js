const Course = require('../models/course');
const { mongooseToObject } = require('../../util/mongoose');
class CourseController {
    // get /course/:slug
    show(req, res, next) {
        Course.findOne({ slug: req.params.slug })
            .then(course => {
                res.render('course/show', { course: mongooseToObject(course) });
            })
            .catch(err => {
                next(err);
            })
    }

    //get /courses/create
    create(req, res, next) {
        res.render('course/create');
    }

    //post /courses/store
    store(req, res, next) {
        const formData = req.body;
        formData.image = `https://img.youtube.com/vi/${req.body.video}/sddefault.jpg`;
        const course = new Course(formData);
        course.save()
            .then(() => {
                res.redirect('/home')
            })
            .catch(err => {
                next();
            })
    }

    //get /courses/:id/edit
    edit(req, res, next) {
        Course.findById(req.params.id)
            .then(course => {
                res.render('course/edit', { course: mongooseToObject(course) });
            })
            .catch(err => {
                next(err);
            })
    }

    //put /courses/:id
    update(req, res, next) {
        Course.updateOne({ _id: req.params.id }, req.body)
            .then(course => {
                res.redirect('/me/stored/courses');
            })
            .catch(err => {
                next(err);
            })
    }

    //delete /courses/:id
    delete(req, res, next) {
        Course.delete({ _id: req.params.id })
            .then(() => {
                res.redirect('back');
            })
            .catch(err => {
                next(err);
            })
    }

    //delete /courses/:id/force
    forceDelete(req, res, next) {
        Course.deleteOne({ _id: req.params.id })
            .then(() => {
                res.redirect('back');
            })
            .catch(err => {
                next(err);
            })
    }

    //patch /courses/:id/restore
    restore(req, res, next) {
        Course.restore({ _id: req.params.id })
            .then(() => {
                res.redirect('back');
            })
            .catch(err => {
                next(err);
            })
    }

    // post /courses/handle-form-actions
    handleFormActions(req, res, next) {
        switch (req.body.action) {
            case 'delete':
                Course.delete({ _id: { $in: req.body.courseIds } })
                    .then(() => {
                        res.redirect('back');
                    })
                    .catch(err => {
                        next(err);
                    })

                break
            default:
                res.json({ message: 'Action is invalid!' });
        }
    }

}

module.exports = new CourseController();
