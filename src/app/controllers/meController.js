const Course = require('../models/course');
const { mongooseToObject } = require('../../util/mongoose');
const { mutipleMongooseToObject } = require('../../util/mongoose');
class meController {
    // get /me/stored/courses
    storedCourses(req, res, next) {
        // let courseQuery = Course.find({});

        Promise.all([
            Course.find({}).sortable(req),
            Course.countDocumentsDeleted()])
            .then(([course, deletedCount]) => {
                res.render('me/stored-courses', {
                    deletedCount,
                    course: mutipleMongooseToObject(course)
                });
            })
            .catch(err => {
                next(err);
            })

        // Course.countDocumentsDeleted()
        //     .then((deletedCount) => {

        //     })
        //     .catch(err => {
        //         next(err);
        //     })


        // Course.find({})
        //     .then(course => {
        //         res.render('me/stored-courses', { course: mutipleMongooseToObject(course) });
        //     })
        //     .catch(err => {
        //         next(err);
        //     })
    }

    // get /me/trash/courses
    trashCourses(req, res, next) {
        Course.findDeleted({})
            .then(course => {
                res.render('me/trash-courses', { course: mutipleMongooseToObject(course) });
            })
            .catch(err => {
                next(err);
            })
    }
}

module.exports = new meController();
