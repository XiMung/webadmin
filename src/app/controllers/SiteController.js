const Course = require('../models/course');
const { mutipleMongooseToObject } = require('../../util/mongoose');
class SiteController {
    // get /
    home(req, res, next) {

        Course.find({})
            .then(course => {
                res.render('home', { course: mutipleMongooseToObject(course) });
            })
            .catch(err => {
                next(err);
            })
        // res.render('home');
    }

    // get /search
    search(req, res) {
        res.render('search');
    }
}

module.exports = new SiteController();
